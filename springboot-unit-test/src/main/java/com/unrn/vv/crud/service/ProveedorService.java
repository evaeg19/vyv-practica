package com.unrn.vv.crud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unrn.vv.crud.model.Proveedor;
import com.unrn.vv.crud.repository.ProveedorRepository;
import java.util.List;

public class ProveedorService {
     @Autowired
    private ProveedorRepository repository;

    public Proveedor saveProveedor(Proveedor proveedor) {
        return repository.save(proveedor);
    }

    public List<Proveedor> saveProveedors(List<Proveedor> proveedors) {
        return repository.saveAll(proveedors);
    }

    public List<Proveedor> getProveedors() throws MyException {
        List<Proveedor> proveedors;
        try {
            proveedors = repository.findAll();
        } catch (Exception e) {
            throw new MyException();
        }
        return proveedors;
    }

    public Proveedor getProveedorById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Proveedor getProveedorByName(String nameProv) {
        return repository.findByNameProv(nameProv);
    }

    public String deleteProveedor(int id) {
        repository.deleteById(id);
        return "Proveedor deleted !! " + id;
    }

    public Proveedor updateProveedor(int proveedorId, Proveedor proveedor) {
        Proveedor existingProveedor = repository.findById(proveedorId).orElse(null);
        existingProveedor.setNameProv(proveedor.getNameProv());
        existingProveedor.setStreet(proveedor.getStreet());
        existingProveedor.setCity(proveedor.getCity());
        existingProveedor.setState(proveedor.getState());
        existingProveedor.setPostalCode(proveedor.getPostalCode());
        return repository.save(existingProveedor);
    }
}
