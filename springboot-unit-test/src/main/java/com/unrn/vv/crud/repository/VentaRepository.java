package com.unrn.vv.crud.repository;

import com.unrn.vv.crud.model.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VentaRepository extends JpaRepository<Venta, Integer> {
}