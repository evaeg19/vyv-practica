package com.unrn.vv.crud.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.unrn.vv.crud.model.Venta;
import com.unrn.vv.crud.repository.VentaRepository;

import antlr.collections.List;

public class VentasService {
     @Autowired
    private VentaRepository repository;

    public Venta saveVenta(Venta venta) {
        return repository.save(venta);
    }

    public List<Venta> saveVentas(List<Venta> ventas) {
        return repository.saveAll(ventas);
    }

    public List<Venta> getVentas() throws MyException {
        List<Venta> ventas;
        try {
            ventas = repository.findAll();
        } catch (Exception e) {
            throw new MyException();
        }
        return ventas;
    }

    public Venta getVentaById(int id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteVenta(int id) {
        repository.deleteById(id);
        return "Venta deleted !! " + id;
    }

    public Venta updateVenta(int ventaId, Venta venta) {
        Venta existingVenta = repository.findById(ventaId).orElse(null);
        existingVenta.setDate(venta.getDate());
        existingVenta.setTotal(venta.getTotal());
        existingVenta.setState(venta.isState());
        return repository.save(existingVenta);
    }
    
}
