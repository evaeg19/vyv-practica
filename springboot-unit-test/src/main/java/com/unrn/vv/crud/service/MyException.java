package com.unrn.vv.crud.service;

public class MyException extends Exception{
    public MyException() {
        super("An error occurred while processing the request.");
    }

    public MyException(String message) {
        super(message);
    }

}
