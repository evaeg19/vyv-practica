package com.unrn.vv.crud.repository;

import com.unrn.vv.crud.model.Proveedor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProveedorRepository extends JpaRepository<Proveedor, Integer> {
    Proveedor findByNameProv(String nameProv);
}
